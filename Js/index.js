$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
      interval: 2000
    });
    $('#contacto').on('show.bs.modal', function(e){
      console.log('El modal contacto se esta mostrando con show');
      $('#contactoBtn').removeClass('btn-outline-success');
      $('#contactoBtn').addClass('btn-outline-danger');
      $('#contactoBtn').prop('disabled', true);
    });

    $('#contacto').on('shown.bs.modal', function(e){
      console.log('El modal contacto se esta mostrando con shown');
    });

    $('#contacto').on('hide.bs.modal', function(e){
      console.log('El modal contacto se esta ocultando con hide');
    });

    $('#contacto').on('hidden.bs.modal', function(e){
      console.log('El modal contacto se esta ocultando don hidden');
      $('#contactoBtn').removeClass('btn-outline-danger');
      $('#contactoBtn').addClass('btn-outline-success');
      $('#contactoBtn').prop('disabled', false);
    });
  });